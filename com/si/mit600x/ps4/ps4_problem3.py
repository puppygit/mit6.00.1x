def loadWords():
    """
    Returns a list of valid words. Words are strings of lowercase letters.

    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open("words.txt", 'r')
    # wordList: list of strings
    wordList = []
    for line in inFile:
        wordList.append(line.strip().lower())
    print("  ", len(wordList), "words loaded.")
    return wordList

wordList = loadWords()

def updateHand(hand, word):
    """
    Assumes that 'hand' has all the letters in word.
    In other words, this assumes that however many times
    a letter appears in 'word', 'hand' has at least as
    many of that letter in it.

    Updates the hand: uses up the letters in the given word
    and returns the new hand, without those letters in it.

    Has no side effects: does not modify hand.

    word: string
    hand: dictionary (string -> int)
    returns: dictionary (string -> int)
    """

    #create a copy of hand dict
    chand = hand.copy()
    #check if letter of the word is in the dictionary and reduce the value by 1 if yes

    for letter in word:
        if letter in chand.keys():
            chand[letter] -=1
    return chand

def isValidWord(word, hand, wordList):
    """
    Returns True if word is in the wordList and is entirely
    composed of letters in the hand. Otherwise, returns False.

    Does not mutate hand or wordList.

    word: string
    hand: dictionary (string -> int)
    wordList: list of lowercase strings
    """

    value = False
    if word not in wordList:
        return False
    else:
        for i in range(len(word)):
            chand=getFrequencyDict(word[i:])
            if word[i] in hand.keys() and chand[word[i]]<=hand[word[i]]:
                value = True
            else:
                value = False
                return value
    return value



hand = {'h': 1, 'e': 1, 'l': 2, 'o': 1}
def getFrequencyDict(sequence):
    """
    Returns a dictionary where the keys are elements of the sequence
    and the values are integer counts, for the number of times that
    an element is repeated in the sequence.

    sequence: string or list
    return: dictionary
    """
    # freqs: dictionary (element_type -> int)
    freq = {}
    for x in sequence:
        freq[x] = freq.get(x,0) + 1
    return freq

hand2 = {'r': 1, 'a': 3, 'p': 2, 'e': 1, 't': 1, 'u': 1}
word2 = 'rapture'
print(isValidWord('rapture', hand2, wordList))

