def calculateHandlen(hand):
    """
    Returns the length (number of letters) in the current hand.
    hand: dictionary (string-> int)
    returns: integer
    """
    handlen = 0
    for item in hand:
        if hand[item]==0:
            continue
        else:
            handlen+=hand[item]
    return handlen

hand = {'h': 1, 'e': 3, 'l': 2, 'o': 1}

print(calculateHandlen(hand))