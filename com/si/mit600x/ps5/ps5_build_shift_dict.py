import string



def build_shift_dict(shift):
    lower_keys = list(string.ascii_lowercase)
    lower_values = list(string.ascii_lowercase)
    shift_lower_values = lower_values[shift:] + lower_values[:shift]

    upper_keys = list(string.ascii_uppercase)
    upper_values = list(string.ascii_uppercase)
    upper_shift_values = upper_values[shift:] + upper_values[:shift]

    full_keys = lower_keys + upper_keys
    full_values = shift_lower_values + upper_shift_values

    shift_dict = dict(zip(full_keys, full_values))
    return shift_dict


def apply_shift(shift,word):
    '''
    wirting here for testing. Main code will be in ps5_encryption.py
    :param shift:
    :param word:
    :return:
    '''
    newWord = ""
    shiftedDict = build_shift_dict(shift)
    for letter in word:
        if letter in string.punctuation or letter == ' ' or letter in string.digits:
            newWord +=letter
        else:
            newWord +=shiftedDict[letter]
    return newWord

newDict1 = build_shift_dict(5)
print(newDict1)
newDict2 = build_shift_dict(3)
print(newDict2)
newDict3 = build_shift_dict(1)
print(newDict3)

#print(apply_shift(3,'apple'))
test = {'R': 'F', 'K': 'A', 'Q': 'V', 'S': 'N', 'r': 'a', 'G': 'J', 'A': 'S', 'J': 'Y', 'w': 'q', 'P': 'Z', 'U': 'H', 'f': 'b', 'h': 'i', 'g': 'm', 'H': 'X', 'b': 'y', 'D': 'W', 'n': 'u', 't': 'v', 'q': 'h', 'O': 'R', 'u': 'w', 'B': 'K', 'W': 'I', 'o': 'j', 'L': 'G', 'p': 'x', 'e': 'l', 'I': 'M', 'X': 'Q', 'F': 'P', 'V': 'T', 'M': 'U', 'j': 'f', 'x': 's', 'l': 'g', 'a': 'k', 'T': 'E', 'y': 'c', 's': 'd', 'Z': 'C', 'N': 'O', 'E': 'B', 'k': 't', 'd': 'n', 'm': 'z', 'C': 'L', 'i': 'o', 'z': 'p', 'v': 'e', 'c': 'r', 'Y': 'D'}

print(len(test.keys()))