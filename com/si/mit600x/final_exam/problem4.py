def lognestRun(L):
    '''
    Write a function called longestRun,
    which takes as a parameter a list of integers named L (assume L is not empty).
    This function returns the length of the longest run of monotonically increasing numbers occurring in L.
    A run of monotonically increasing numbers means that a number at position k+1
    in the sequence is either greater than or equal to the number at position k in the sequence.

    For example, if L = [10, 4, 6, 8, 3, 4, 5, 7, 7, 2] then your function should return the value 5
    because the longest run of monotonically increasing integers in L is [3, 4, 5, 7, 7].
    :param L: list of integers
    :return: int - longest run of monotonically increasing integers in L
    '''
    resultList = []
    temp = resultList[:]
    for i in range(len(L)):

        if i == 0:
            temp.append(L[i])
        elif L[i] >= L[i-1]:
            temp.append(L[i])

        else:
            temp = []
            temp.append(L[i])
        if len(temp) >= len(resultList):
            resultList = temp[:]

    return len(resultList)

L = [10, 4, 6, 8, 3, 4, 5, 7, 7, 2]
L1 = [0]
L2 = [1,1,1,1,1]
L3 = [-10, -5, 0, 5, 10]
print(lognestRun(L))
print(lognestRun(L1))
print(lognestRun(L2))
print(lognestRun([1, 3, 5, -1, -3, -5, -7, 1, 3, 5]))
print(lognestRun(L3))
