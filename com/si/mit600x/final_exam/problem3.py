def print_without_vowels(s):
    '''
    s: the string to convert
    Finds a version of s without vowels and whose characters appear in the
    same order they appear in s. Prints this version of s.
    Does not return anything
    '''
    # Your code here
    vowels = 'aeiouAEIOU'
    temp_s = ""
    for letter in s:
        if letter in vowels:
            letter = ''
            temp_s +=letter
        else:
            temp_s += letter
    print(temp_s)


print_without_vowels('Hello, World! OlalA')
