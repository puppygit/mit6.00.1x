As written, this code leads to an infinite loop when using the Arrogant Professor class.

Change the definition of ArrogantProfessor so that the following behavior is achieved:

e = Person('eric')
le = Lecturer('eric')
pe = Professor('eric')
ae = ArrogantProfessor('eric')

>>> e.say('the sky is blue')
eric says: the sky is blue

>>> le.say('the sky is blue')
eric says: the sky is blue

>>> le.lecture('the sky is blue')
I believe that eric says: the sky is blue

>>> pe.say('the sky is blue')
eric says: I believe that eric says: the sky is blue

>>> pe.lecture('the sky is blue')
I believe that eric says: the sky is blue

>>> ae.say('the sky is blue')
eric says: It is obvious that eric says: the sky is blue

>>> ae.lecture('the sky is blue')
It is obvious that eric says: the sky is blue
Paste ONLY your ArrogantProfessor class in the box below. Do not leave any debugging print statements.