def f(a,b):
    return a>b


def dict_interdiff(d1,d2):

    dict_unique = {}
    dict_both = {}

    d1_keys = list(d1.keys())
    d2_keys = list(d2.keys())



    for key in d1_keys:
        if key in d2_keys and key:
            dict_both[key] = f(d1[key],d2[key])
        else:
            dict_unique[key]=d1[key]

    for key in d2_keys:
        if key not in list(dict_both.keys()):
            dict_unique[key] = d2[key]


    return (dict_both,dict_unique)

d1 = {1:30, 2:20, 3:30, 5:80}
d2 = {1:40, 2:50, 3:60, 4:70, 6:90}

d3 = {1:30, 2:20, 3:30}
d4 = {1:40, 2:50, 3:60}

d5 = {}
d6 = {1:40, 2:50, 3:60}

d7 = {1:10, 2:60, 3:70}
d8 = {}
print(dict_interdiff(d1,d2))
print(dict_interdiff(d3,d4))
print(dict_interdiff(d5,d8))
print(dict_interdiff(d7,d8))
