import string
def getAvailableLetters(lettersGuessed):
    '''
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters that represents what letters have not
      yet been guessed.
    '''
    # FILL IN YOUR CODE HERE...
    lower_letters = string.ascii_lowercase
    output_str = string.ascii_lowercase
    if len(lettersGuessed) == 0:
        return lower_letters
    for letter in lettersGuessed:
        output_str = output_str.replace(letter,"")
    return output_str

lettersGuessed = ['e', 'i', 'k', 'p', 'r', 's']

check = 'abcdfghjlmnoqtuvwxyz'
result = getAvailableLetters(lettersGuessed)

print(check==result)