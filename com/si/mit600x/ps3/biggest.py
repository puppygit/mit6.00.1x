animals = { 'a': ['aardvark'], 'b': ['baboon'], 'c': ['coati']}

animals['d'] = ['donkey']
animals['d'].append('dog')
animals['d'].append('dingo')


def biggest(aDict):
    '''
    aDict: A dictionary, where all the values are lists.

    returns: The key with the largest number of values associated with it
    '''
    # Your Code Here
    maximum = None
    length = 0
    length_prev = 0
    if len(aDict) == 0:
        return None
    for index in aDict:
        length = len(aDict[index])
        if length>length_prev:
            length_prev = length
            maximum = index
        else:
            continue

    return maximum


print(biggest(animals))
