def oddTuples(aTup):
    '''
    aTup: a tuple

    returns: tuple, every other element of aTup.
    '''
    # Your Code Here
    result = ()
    index = 0
    while index <= len(aTup)-1:
        result = result + (aTup[index],)
        index = index+2
    return result

oTuple = oddTuples((12,))
print(oTuple)