def getGuessedWord(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters and underscores that represents
      what letters in secretWord have been guessed so far.
    '''
    # FILL IN YOUR CODE HERE...
    output_str = ""
    for letter in secretWord:
        if letter in lettersGuessed:
            output_str = output_str+letter
        else:
            output_str = output_str + "_ "
    return output_str

sw = 'apple'
lg = ['e', 'i', 'k', 'p', 'r', 's']
print(getGuessedWord(sw,lg))