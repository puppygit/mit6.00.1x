animals = { 'a': ['aardvark'], 'b': ['baboon'], 'c': ['coati']}

animals['d'] = ['donkey']
animals['d'].append('dog')
animals['d'].append('dingo')


def how_many(aDict):
    '''
    aDict: A dictionary, where all the values are lists.

    returns: int, how many values are in the dictionary.
    '''
    # Your Code Here
    length = 0
    for index in aDict:
        if type(aDict[index]==list):
            length = length+len(aDict[index])
        else:
            length = length+1
    return length
print(how_many(animals))