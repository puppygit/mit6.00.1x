def isWordGuessed(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: boolean, True if all the letters of secretWord are in lettersGuessed;
      False otherwise
    '''
    # FILL IN YOUR CODE HERE...
    if len(lettersGuessed) == 0:
        return False
    for i in range(len(lettersGuessed)):
        if lettersGuessed[i] in secretWord:
            continue
        elif lettersGuessed[i] not in secretWord and lettersGuessed[i]!=lettersGuessed[-1]:
            continue
        else:
            return False
    return True
sw = 'grapefruit'
lg = ['z', 'x', 'q', 'g', 'r', 'a', 'p', 'e', 'f', 'r', 'u', 'i', 't']
print(isWordGuessed(sw,lg))