def count7(N):
    '''
    N: a non-negative integer
    '''
    # Your code here


    if N//10 == 0 and N!=7:
        return 0
    if N//10 == 0 and N==7:
        return 1
    elif N%10 ==7:
            return 1 + count7(N//10)
    else:
        return count7(N//10)
print(count7(123712773))