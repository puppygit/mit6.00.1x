d = {1:10, 2:20, 3:30}
d = {4:True, 2:True, 0:True}
#d = {1: 3, 2: 4}
d={8: 6, 2: 6, 4: 6, 6: 6}

def dict_invert(d):
    '''
    d: dict
    Returns an inverted dictionary according to the instructions above
    '''
    # Your code here
    d_new = {}
    for key,val in d.items():
        d_new.setdefault(val, []).append(key)
    for k in d_new:
        d_new[k].sort()
    return d_new

print(dict_invert(d))