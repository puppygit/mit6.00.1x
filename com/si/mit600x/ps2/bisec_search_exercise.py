print("Please think of a number between 0 and 100!")
init_num = 50
low = 0
high = 100
#num = int((high - low) / 2)
num = init_num
while True:

    print("Is your secret number " + str(num) + "?")
    #print("Enter 'h' to indicate the guess is too high. Enter 'l' to indicate the guess is too low. Enter 'c' to indicate I guessed correctly.")
    response = input("Enter 'h' to indicate the guess is too high. Enter 'l' to indicate the guess is too low. Enter 'c' to indicate I guessed correctly.")
    if response == 'h':
        high = num
        num = int(num - ((high - low) / 2))
    elif response == 'l':
        low = num
        num = int(num + ((high - low) / 2))
    elif response =='c':
        print("Game over. Your secret number was: " + str(num))
        break
    else:
        print("Sorry, I did not understand your input.")