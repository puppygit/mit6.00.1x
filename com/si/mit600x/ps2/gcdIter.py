def gcdIter(a, b):
    '''
    a, b: positive integers

    returns: a positive integer, the greatest common divisor of a & b.
    '''
    # Your code here
    if a>b:
        smaller = b
    elif b>a:
        smaller = a
    else:
        return a

    while smaller>0:
        if a%smaller == 0 and b%smaller ==0:
            return smaller
        smaller -=1

print(gcdIter(17,12))