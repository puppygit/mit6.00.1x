def isIn(char, aStr):
    '''
    char: a single character
    aStr: an alphabetized string

    returns: True if char is in aStr; False otherwise
    '''
    # Your code here
    if aStr =='':
        return False
    if char > aStr[-1] or char < aStr[0]:
        return False
    aLen = len(aStr)
    if char == aStr[aLen//2]:
        return True
    elif char < aStr[aLen//2]:
        aStr = aStr[0:aLen//2]
    else:
        aStr = aStr[aLen//2:]
    return isIn(char, aStr)
print(isIn('a','bcdef'))