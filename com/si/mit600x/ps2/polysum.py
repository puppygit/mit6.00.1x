import math
def polysum(n,s):
    """
    This function returns the sum of the area and square of the perimeter of the regular polygon.
    The function returns the sum, rounded to 4 decimal places.
    :param n: number of sides of the polygon
    :param s: length of the side
    :return: psum = area + perimeter^2
    """
    #pi = 3.1415

    area = round((0.25*n*(s**2))/(math.tan(math.pi/n)),4)
    perimeter = round((n*s),4)
    psum = area + perimeter**2
    psum = round(psum, 4)
    return psum
