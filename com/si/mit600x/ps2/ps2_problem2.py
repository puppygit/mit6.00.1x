"""
Monthly interest rate = (Annual interest rate) / 12.0
Monthly unpaid balance = (Previous balance) - (Minimum fixed monthly payment)
Updated balance each month = (Monthly unpaid balance) + (Monthly interest rate x Monthly unpaid balance)
"""
balance = 3329
annualInterestRate = 0.2
monthlyInterestRate = annualInterestRate/12.0
minimumFixedPayment = 10
monthlyUnpaidBalance = balance - minimumFixedPayment


upd_balance = balance
interest = 0

while monthlyUnpaidBalance > 0:
    month = 0
    while month <= 11:
        monthlyUnpaidBalance = upd_balance - minimumFixedPayment
        upd_balance = monthlyUnpaidBalance + (monthlyInterestRate*monthlyUnpaidBalance)
        month += 1
    if upd_balance > 0:
        minimumFixedPayment += 10
        upd_balance = balance
print("Lowest Payment: " + str(minimumFixedPayment))

