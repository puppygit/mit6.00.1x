def iterPower(base, exp):
    '''
    base: int or float.
    exp: int >= 0

    returns: int or float, base^exp
    '''
    # Your code here
    i = 0
    result = base
    if exp == 0:
        return 1
    for i in range(1,exp):
        result = result*base
    return result



print(iterPower(2,10))