"""
Monthly interest rate = (Annual interest rate) / 12.0
Monthly payment lower bound = Balance / 12
Monthly payment upper bound = (Balance x (1 + Monthly interest rate)12) / 12.0
"""
balance = 320000
annualInterestRate = 0.2
# Paste your code into this box
monthlyInterestRate = annualInterestRate/12.0
monthlyPaymentLowerBound = balance/12
monthlyPaymentUpperBound = (balance*(1+monthlyInterestRate)**12)/12.0

minimumFixedPayment = (monthlyPaymentUpperBound + monthlyPaymentLowerBound)/2.0
monthlyUnpaidBalance = balance - minimumFixedPayment


upd_balance = balance
interest = 0

while upd_balance>=0.1:
    month = 0
    minimumFixedPayment = (monthlyPaymentUpperBound + monthlyPaymentLowerBound) / 2.0
    while month <= 11:
        monthlyUnpaidBalance = upd_balance - minimumFixedPayment
        upd_balance = monthlyUnpaidBalance + (monthlyInterestRate*monthlyUnpaidBalance)
        month += 1
    if upd_balance<0:
        monthlyPaymentUpperBound = minimumFixedPayment
        upd_balance = balance
    elif (upd_balance>0.1):
        #monthlyPaymentLowerBound = balance/12
        monthlyPaymentLowerBound = minimumFixedPayment
        upd_balance = balance
print("Lowest Payment: " + str(round(minimumFixedPayment,2)))
