balance = 484
annualInterestRate = 0.2
monthlyPaymentRate = 0.04
monthlyInterestRate = annualInterestRate/12

month = 0

while month<=11:
    minimumMonthlyPayment = monthlyPaymentRate*balance
    monthlyUnpaidBalance = balance - minimumMonthlyPayment
    balance = monthlyUnpaidBalance + monthlyInterestRate*monthlyUnpaidBalance
    month +=1
print("Remaining balance: " + str(round(balance,2)))