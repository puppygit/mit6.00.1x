# MIT6.00x

edX course MITx: 6.00.1x Introduction to Computer Science and Programming Using Python(Fall 2017)
https://courses.edx.org/courses/course-v1:MITx+6.00.1x+2T2017_2/course/

Solutions to some finger exercises, all problem sets, midterm and final exam.
